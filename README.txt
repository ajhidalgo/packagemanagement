PACKAGE MANAGEMENT - API

 - Installation:

	1) Export to WAR
	2) Deploy on Tomcat 7.

 - Use:

	- Use Postman (or other client for REST messaging) to send the messages below.
	- Basic auth: user/pass
	- Body returns JSON.

/=======|===============================================================================|===============================================\
|Method	|					URL					|			Comment			|
|=======|===============================================================================|===============================================|
|GET	|http://domain[:port]/PackageManagement/PackageService/				|Gets 'index.jsp'				|
|-------|-------------------------------------------------------------------------------|-----------------------------------------------|
|POST	|http://domain[:port]/PackageManagement/PackageService/				|Create new Package				|
|-------|-------------------------------------------------------------------------------|-----------------------------------------------|
|PUT	|http://domain[:port]/PackageManagement/PackageService/				|Update Package by id				|
|-------|-------------------------------------------------------------------------------|-----------------------------------------------|
|DELETE	|http://domain[:port]/PackageManagement/PackageService/				|Delete Package by id				|
|-------|-------------------------------------------------------------------------------|-----------------------------------------------|
|GET	|http://domain[:port]/PackageManagement/PackageService/get/{id}			|Gets Package by id				|
|-------|-------------------------------------------------------------------------------|-----------------------------------------------|
|GET	|http://domain[:port]/PackageManagement/PackageService/get/{id}/{currency}	|Gets Package by id with specific currency	|
|-------|-------------------------------------------------------------------------------|-----------------------------------------------|
|GET	|http://domain[:port]/PackageManagement/PackageService/getAll			|Gets all packages				|
|-------|-------------------------------------------------------------------------------|-----------------------------------------------|
|GET	|http://domain[:port]/PackageManagement/PackageService/getAll/{currency}	|Gets all packages with specific currency	|
\=======|===============================================================================|===============================================/


