/**
 * 
 */
package com.project.pm.config;

import java.util.Arrays;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * User authentication manager.
 * 
 * @author ajhidalgo
 *
 */
@Service
public class UserAuthManager implements UserDetailsService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetails userDetails = (UserDetails) new org.springframework.security.core.userdetails.User("user", "pass",
				Arrays.asList());
		return userDetails;
	}

}
