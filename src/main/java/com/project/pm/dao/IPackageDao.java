/**
 * 
 */
package com.project.pm.dao;

import java.util.List;

import com.project.pm.entities.Package;

/**
 * Interface of Package DAO.
 * 
 * @author ajhidalgo
 *
 */
public interface IPackageDao {

	/**
	 * Create new package.
	 * 
	 * @param pck
	 */
	public void persist(Package pck);

	/**
	 * Update package, found by id.
	 * 
	 * @param pck
	 */
	public void update(Package pck);

	/**
	 * Delete package, found by id.
	 * 
	 * @param pck
	 */
	public void delete(Package pck);

	/**
	 * Find package by id.
	 * 
	 * @param id
	 * @return
	 */
	public Package findById(String id);

	/**
	 * Find all the packages.
	 * 
	 * @return
	 */
	public List<Package> findAll();

}
