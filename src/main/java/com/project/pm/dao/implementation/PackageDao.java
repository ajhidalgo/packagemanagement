/**
 * 
 */
package com.project.pm.dao.implementation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.stereotype.Repository;

import com.project.pm.dao.IPackageDao;
import com.project.pm.entities.Package;

/**
 * Implementation of Package DAO.
 * 
 * @author ajhidalgo
 *
 */
@Repository
public class PackageDao implements IPackageDao {
	
	/** BBDD. */
	private static HashMap<String, Package> packages;
	
	/**
	 * Constructor.
	 */
	public PackageDao() {
		packages = new HashMap<String, Package>();
	}

	@Override
	public void persist(Package pck) {
		packages.put(pck.getId(), pck);
	}

	@Override
	public void update(Package pck) {
		packages.put(pck.getId(), pck);
	}

	@Override
	public void delete(Package pck) {
		packages.remove(pck.getId());
	}

	@Override
	public Package findById(String id) {
		return packages.get(id);
	}

	@Override
	public List<Package> findAll() {
		List<Package> result = new ArrayList<Package>();
		
		for (Iterator<Entry<String, Package>> it = packages.entrySet().iterator(); it.hasNext();) {
			Entry<String, Package> next = it.next();
			result.add(next.getValue());
		}
		return result;
	}


}
