/**
 * 
 */
package com.project.pm.entities;

import java.io.Serializable;
import java.util.Set;

/**
 * @author ajhidalgo
 *
 */
public class Package implements Serializable {

	/** Serial version. */
	private static final long serialVersionUID = 1L;

	/** Id. */
	private String id;

	/** Name. */
	private String name;

	/** Description. */
	private String description;

	/** Products. */
	private Set<Product> products;

	/** Sum of all of the products, in USD cents. */
	private Integer usdPrice;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the products
	 */
	public Set<Product> getProducts() {
		return products;
	}

	/**
	 * @param products
	 *            the products to set
	 */
	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	/**
	 * @return the usdPrice
	 */
	public Integer getUsdPrice() {
		return usdPrice;
	}

	/**
	 * @param usdPrice
	 *            the usdPrice to set
	 */
	public void setUsdPrice(Integer usdPrice) {
		this.usdPrice = usdPrice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Package other = (Package) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (products == null) {
			if (other.products != null)
				return false;
		} else if (!products.equals(other.products))
			return false;
		if (usdPrice == null) {
			if (other.usdPrice != null)
				return false;
		} else if (!usdPrice.equals(other.usdPrice))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Package [id=" + id + ", name=" + name + ", description=" + description + ", products=" + products
				+ ", usdPrice=" + usdPrice + "]";
	}

}
