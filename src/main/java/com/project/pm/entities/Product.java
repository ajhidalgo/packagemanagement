/**
 * 
 */
package com.project.pm.entities;

import java.io.Serializable;

/**
 * Product entity.
 * 
 * @author ajhidalgo
 *
 */
public class Product implements Serializable {

	/** Serial version. */
	private static final long serialVersionUID = 1L;

	/** Id. */
	private String id;

	/** Name. */
	private String name;

	/** Price in USD cents. */
	private Integer usdPrice;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the usdPrice
	 */
	public Integer getUsdPrice() {
		return usdPrice;
	}

	/**
	 * @param usdPrice
	 *            the usdPrice to set
	 */
	public void setUsdPrice(Integer usdPrice) {
		this.usdPrice = usdPrice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (usdPrice == null) {
			if (other.usdPrice != null)
				return false;
		} else if (!usdPrice.equals(other.usdPrice))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", usdPrice=" + usdPrice + "]";
	}

}
