/**
 * 
 */
package com.project.pm.manager;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.web.client.RestClientException;

import com.project.pm.entities.Package;

/**
 * Interface of the package manager.
 * 
 * @author ajhidalgo
 *
 */
public interface IPackageManager {

	/**
	 * Creates a new package.
	 * 
	 * @param pck
	 */
	public void create(Package pck);

	/**
	 * Update a package.
	 * 
	 * @param pck
	 */
	public void update(Package pck);

	/**
	 * Delete a package.
	 * 
	 * @param pck
	 */
	public void delete(Package pck);

	/**
	 * Find a package by id. If currency is null, default is USD.
	 * 
	 * @param id
	 * @param currency
	 * @return
	 * @throws RestClientException
	 * @throws UnsupportedEncodingException
	 */
	public Package findById(String id, String currency) throws RestClientException, UnsupportedEncodingException;

	/**
	 * Find all the packages. If currency is null, default is USD.
	 * 
	 * @param currency
	 * @return
	 * @throws RestClientException
	 * @throws UnsupportedEncodingException
	 */
	public List<Package> findAll(String currency) throws RestClientException, UnsupportedEncodingException;

}
