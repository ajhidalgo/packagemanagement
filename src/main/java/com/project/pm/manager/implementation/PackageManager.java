/**
 * 
 */
package com.project.pm.manager.implementation;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.project.pm.dao.IPackageDao;
import com.project.pm.entities.Currency;
import com.project.pm.entities.Package;
import com.project.pm.entities.Product;
import com.project.pm.manager.IPackageManager;

/**
 * Implementation of the package manager.
 * 
 * @author ajhidalgo
 *
 */
@Service
public class PackageManager implements IPackageManager {

	@Autowired
	private IPackageDao iPackageDao;

	@Override
	public void create(Package pck) {
		this.iPackageDao.persist(pck);
	}

	@Override
	public void update(Package pck) {
		this.iPackageDao.update(pck);
	}

	@Override
	public void delete(Package pck) {
		this.iPackageDao.delete(pck);
	}

	@Override
	public Package findById(String id, String currency) throws RestClientException, UnsupportedEncodingException {
		Package pck = this.iPackageDao.findById(id);
		pck.setUsdPrice((int) updateCurrency(currency, pck));
		return pck;
	}

	@Override
	public List<Package> findAll(String currency) throws RestClientException, UnsupportedEncodingException {
		List<Package> result = this.iPackageDao.findAll();
		for (Package pck : result) {
			pck.setUsdPrice((int) updateCurrency(currency, pck));
		}
		return result;
	}

	/**
	 * Updates the price of the package to the selected currency.
	 * 
	 * @param currency
	 * @param pck
	 * @return
	 * @throws RestClientException
	 * @throws UnsupportedEncodingException
	 */
	private float updateCurrency(String currency, Package pck)
			throws RestClientException, UnsupportedEncodingException {
		float lastCurrency = 1.0f;
		if (currency != null) {
			lastCurrency = getCurrency(currency);
		}
		float total = 0.0f;
		if (pck.getProducts() != null) {
			for (Product pro : pck.getProducts()) {
				total += pro.getUsdPrice() * lastCurrency;
			}
		}
		return total;
	}

	/**
	 * Gets the current currenty from the API Fixer.
	 * 
	 * @param currency
	 * @return
	 * @throws RestClientException
	 * @throws UnsupportedEncodingException
	 */
	private float getCurrency(String currency) throws RestClientException, UnsupportedEncodingException {
		try {
			RestTemplate restTemplate = new RestTemplate();
			String url = "http://api.fixer.io/latest?base=USD&symbols=" + currency;
			url = URLDecoder.decode(url, "UTF-8");

			ResponseEntity<Currency> exchange = restTemplate.getForEntity(url, Currency.class);
			Currency body = exchange.getBody();
			return body.getRates().get(currency);
		} catch (RestClientException | UnsupportedEncodingException e) {
			System.err.println(e.getMessage());
			throw e;
		}
	}

}
