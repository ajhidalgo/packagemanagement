/**
 * 
 */
package com.project.pm.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestClientException;

import com.project.pm.entities.Package;
import com.project.pm.manager.IPackageManager;

/**
 * RESTful Web Services.
 * 
 * @author ajhidalgo
 *
 */
@Controller
@RequestMapping("PackageService")
public class PackageService {

	@Autowired
	private IPackageManager iPackageManager;
	
	@GetMapping
	public String home() {
		return "index";
	}
	
	@PostMapping
	public ResponseEntity<Void> create(@RequestBody Package pck) {
		try {
			this.iPackageManager.create(pck);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}

	@PutMapping
	public ResponseEntity<Void> update(@RequestBody Package pck) {
		try {
			this.iPackageManager.update(pck);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}

	@DeleteMapping
	public ResponseEntity<Void> delete(@RequestBody Package pck) {
		try {
			this.iPackageManager.delete(pck);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}

	@GetMapping("get/{id}")
	public ResponseEntity<Package> find(@PathVariable("id") String id) {
		try {
			Package pck = this.iPackageManager.findById(id, null);
			return new ResponseEntity<Package>(pck, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Package>(HttpStatus.CONFLICT);
		}
	}

	@GetMapping("get/{id}/{currency}")
	public ResponseEntity<Package> findCurrency(@PathVariable("id") String id,
			@PathVariable("currency") String currency) {
		try {
			Package pck = this.iPackageManager.findById(id, currency);
			return new ResponseEntity<Package>(pck, HttpStatus.OK);
		} catch (RestClientException e) {
			return new ResponseEntity<Package>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Package>(HttpStatus.CONFLICT);
		}
	}

	@GetMapping("getAll")
	public ResponseEntity<List<Package>> findAll() {
		try {
			List<Package> pck = this.iPackageManager.findAll(null);
			return new ResponseEntity<List<Package>>(pck, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Package>>(HttpStatus.CONFLICT);
		}
	}

	@GetMapping("getAll/{currency}")
	public ResponseEntity<List<Package>> findAllCurrency(@PathVariable("currency") String currency) {
		try {
			List<Package> pck = this.iPackageManager.findAll(currency);
			return new ResponseEntity<List<Package>>(pck, HttpStatus.OK);
		} catch (RestClientException e) {
			return new ResponseEntity<List<Package>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<List<Package>>(HttpStatus.CONFLICT);
		}
	}

}
