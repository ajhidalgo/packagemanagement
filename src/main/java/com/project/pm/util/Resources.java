/**
 * 
 */
package com.project.pm.util;

/**
 * Constants and enums.
 * 
 * @author ajhidalgo
 *
 */
public class Resources {

	/** Name of the app. */
	public static final String APP_NAME = "PackageManagement";

}
