var rootURL = "http://localhost:8080/PackageManagement/PackageService/";

function findAll() {
	console.log('findAll');
	$.ajax({
		type: 'GET',
		url: rootURL + 'getAll/',
		dataType: "json",
		success: renderList
	});
}

function renderList(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [data]);

	$.each(list, function(index, package) {
		$('#list').append('<tr>');
		$('#list').append('<td>' + package.id + '</td>');
		$('#list').append('<td>' + package.name + '</td>');
		$('#list').append('<td>' + package.description + '</td>');
		$('#list').append('<td>' + package.usdPrice + '</td>');
		$('#list').append('<td>' + package.products != null ? package.products.length : 0 + '</td>');
		$('#list').append('</tr>');
	});
}


