<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Package Management</title>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="/PackageManagement/js/rest.js" type="text/javascript"></script>
</head>
<body>
	<script>
		findAll()
	</script>
	<h1>Package Management</h1>
	<h2>List</h2>
	<table id="result">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Description</th>
				<th>Price</th>
				<th>Number of products</th>
			</tr>
		</thead>
		<tbody id="list"></tbody>
	</table>
</body>
</html>