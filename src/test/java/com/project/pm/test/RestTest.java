package com.project.pm.test;

import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.project.pm.entities.Package;
import com.project.pm.entities.Product;

public class RestTest {

	@Test
	public void testCreate1() {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/PackageManagement/PackageService/";

		Package pck = new Package();
		pck.setId("test1");
		pck.setName("name test1");
		pck.setDescription("description test1");
		Set<Product> products = new HashSet<Product>();
		Product prod = new Product();
		prod.setId("prod1");
		prod.setName("name prod1");
		prod.setUsdPrice(1455);
		products.add(prod);
		pck.setProducts(products);

		HttpHeaders headers = getHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<Package> userEntity = new HttpEntity<Package>(pck, headers);

		restTemplate.postForLocation(url, userEntity);

	}

	@Test
	public void testCreate2() {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/PackageManagement/PackageService/";

		Package pck = new Package();
		pck.setId("test2");
		pck.setName("name test2");
		pck.setDescription("description test2");
		Set<Product> products = new HashSet<Product>();
		Product prod2 = new Product();
		prod2.setId("prod2");
		prod2.setName("name prod2");
		prod2.setUsdPrice(78925);
		products.add(prod2);
		Product prod3 = new Product();
		prod3.setId("prod3");
		prod3.setName("name prod3");
		prod3.setUsdPrice(425);
		products.add(prod3);
		pck.setProducts(products);

		HttpHeaders headers = getHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<Package> userEntity = new HttpEntity<Package>(pck, headers);

		restTemplate.postForLocation(url, userEntity);

	}

	private HttpHeaders getHeaders() {
		String credential = "user:pass";
		String encodedCredential = new String(Base64.getEncoder().encodeToString(credential.getBytes()));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Basic " + encodedCredential);
		return headers;
	}

}
